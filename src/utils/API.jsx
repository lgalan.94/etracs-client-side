export const getFeatures = async () => {
  const response = await fetch(`${import.meta.env.VITE_API_URL}/features/all`, {
    method: 'GET',
    headers: {
      'Content-type': 'application/json'
    }
  });
  return response.json();
};

export const getContact = async () => {
  const response = await fetch(`${import.meta.env.VITE_API_URL}/socials/get`, {
    method: 'GET',
    headers: {
      'Content-type': 'application/json'
    }
  });
  return response.json();
};

export const getAllSettings = async () => {
  const response = await fetch(`${import.meta.env.VITE_API_URL}/settings/settings-all`, {
    method: 'GET',
    headers: {
      'Content-type': 'application/json'
    }
  });
  return response.json();
};

