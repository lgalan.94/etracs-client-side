import { TopBar, NavBar, Footer, Loading } from '../components';
import { getAllSettings } from '../utils/API';
import { useState, useEffect } from 'react';

const About = () => {

  const [about, setAbout] = useState('');
  const [loading, setLoading] = useState(true);

  useEffect(() => {
        getAllSettings()
        .then((data) => {
          const aboutSetting = data.find((setting) => setting.key === 'about'); 
          if (aboutSetting) {
            setAbout(aboutSetting.value);
          }

          setLoading(false);

        });
    }, []);


	return (
		<>
		<TopBar />
		<NavBar />

    {
      loading ? <Loading /> : (

                <div className="bg-gray-100 min-h-screen">
                 <div className="container mx-auto px-4 py-8">
                   <h1 className="text-3xl font-bold mb-4">About Us</h1>
                   <p className="text-gray-700 mb-4">
                     {about}
                   </p>
                 </div>
               </div>

        )
    }

		
		<Footer />
		</>
	)
}

export default About;