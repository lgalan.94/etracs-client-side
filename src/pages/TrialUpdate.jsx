import { useState, useEffect } from 'react';
import { SettingsCard } from '../components';

const TrialUpdate = () => {
  const [settings, setSettings] = useState([]);

  useEffect(() => {
    fetch('http://localhost:9000/settings/settings-all')
      .then((result) => result.json())
      .then((data) => {
          setSettings(data.map((settings) => (
            <SimpleCard key={settings._id} settingsProp={settings} />
          )));
      });
  }, []);

  return <>{settings}</>;
};

export default TrialUpdate;