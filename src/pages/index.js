import Home from './Home';
import TrialUpdate from './TrialUpdate';
import About from './About';

export {
	Home,
	TrialUpdate,
	About
}