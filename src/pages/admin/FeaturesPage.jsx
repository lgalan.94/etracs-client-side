import { AdminSideNavbar, AdminTopbar, AdminFooter } from '../../components';
import { Features } from './contents';

 const FeaturesPage = () => {
  return (
    <>
    <div className="flex h-screen">
      <div className="flex-grow">
        {/* Top Bar */}
        <AdminTopbar />  
        <AdminSideNavbar />
        <p className="px-10 py-6 text-md font-bold text-orange-700 text-xl underline decoration-4 underline-offset-4">Features Section</p>
        <div className="px-10">
          <Features />
        </div>
      </div>
    </div>
    <AdminFooter />
    </>
  );
};

export default FeaturesPage;