import { AdminSideNavbar, AdminTopbar } from '../../components';
import { ToastContainer, toast } from 'react-toastify';
import { BsArrowLeftShort } from "react-icons/bs";

import React, { useState, useEffect } from 'react';
import { useParams, useNavigate, Link } from 'react-router-dom';

const UpdateFeaturePage = () => {
  const navigate = useNavigate();
  const { featureId } = useParams();
  const [title, setTitle] = useState('');
  const [description, setDescription] = useState('');
  const [icon, setIcon] = useState('');
  const [isDirty, setIsDirty] = useState(false);

  useEffect(() => {
    fetch(`${import.meta.env.VITE_API_URL}/features/${featureId}`)
      .then((result) => result.json())
      .then((data) => {
        setTitle(data.title);
        setDescription(data.description);
        setIcon(data.icon);
      });
  }, [featureId]);

  const handleSubmit = (event) => {
    event.preventDefault();

    fetch(`${import.meta.env.VITE_API_URL}/features/${featureId}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        title,
        description,
        icon
      })
    })
      .then(response => response.json())
      .then(data => {
        if (data === true) {
          toast.success("Update Successful!");
          setTimeout(() => navigate('/admin/features'), 700);
        } else {
          toast.error('Error updating settings!');
          setTimeout(() => 700);
        }
      });
  };

  const handleTitleChange = (e) => {
    setTitle(e.target.value);
    setIsDirty(true);
  };

  const handleDescriptionChange = (e) => {
    setDescription(e.target.value);
    setIsDirty(true);
  };

  const handleIconChange = (e) => {
    setIcon(e.target.value);
    setIsDirty(true);
  };

  return (
    <div className="flex h-screen">
      {/* Side Navigation Bar */}
      
      {/* Main Content */}
      <div className="flex-grow">
        {/* Top Bar */}
        <AdminTopbar />
        <AdminSideNavbar />
        <h1 className="p-2"></h1>
        <div className="p-20">
          <p className="text-lg text-orange-600 font-bold mb-4">UPDATE FEATURE</p>
          <div className="shadow-md shadow-stone-500 bg-gray-50 p-4">
          <Link as={Link} to="/admin/features" >
            <button
              type="submit"
              className=" 
                        text-xs
                        hover:bg-gray-500 
                        hover:text-white 
                        rounded-sm p-1 mb-3" 
                        
            >
              <BsArrowLeftShort size={22}/>
            </button>
          </Link>
            <form onSubmit={handleSubmit}>
              <div className="mb-4">
                <label htmlFor="title" className="text-sm font-semibold">
                  Title
                </label>
                <input
                  type="text"
                  id="title"
                  className="focus:outline-none focus:ring-2 focus:ring-blue-500 border border-gray-300 px-2 py-1 rounded-sm w-full"
                  value={title}
                  onChange={handleTitleChange}
                />
              </div>
              <div className="mb-4">
                <label htmlFor="description" className="text-sm font-semibold">
                  Description
                </label>
                <input
                  type="text"
                  id="description"
                  className="focus:outline-none focus:ring-2 focus:ring-blue-500 border border-gray-300 px-2 py-1 rounded-sm w-full"
                  value={description}
                  onChange={handleDescriptionChange}
                />
              </div>
              <div className="mb-4">
                <label htmlFor="icon" className="text-sm font-semibold">
                  Icon
                </label>
                <input
                  type="text"
                  id="icon"
                  className="focus:outline-none focus:ring-2 focus:ring-blue-500 border border-gray-300 px-2 py-1 rounded-sm w-full"
                  value={icon}
                  onChange={handleIconChange}
                />
                <small className="p-1">Visit react-icons <a className="text-blue-600" href="https://react-icons.github.io/react-icons" target="_blank"> here </a></small> 
              </div>
              {isDirty ? (
                <button
                  type="submit"
                  className="outline 
                            outline-slate-300 
                            text-xs
                            hover:bg-gray-500 
                            hover:text-white 
                            rounded-sm px-2 py-1 
                            outline-2"
                >
                  Save
                </button>
              ) : (
                <p

                  className="w-32 text-center outline 
                            outline-slate-300 
                            text-xs
                            hover:bg-gray-500 
                            hover:text-white 
                            rounded-sm px-2 py-1 
                            outline-2"
                >
                  No changes made
                </p>
              )}
            </form>
          </div>
        </div>
      </div>

      <ToastContainer position="top-right" autoClose={5000} hideProgressBar={false} newestOnTop={false} closeOnClick rtl={false} pauseOnFocusLoss draggable pauseOnHover theme="light" />
    </div>
  );
};

export default UpdateFeaturePage;
