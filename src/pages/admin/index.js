import HomePage from './HomePage';
import UpdateSettingsPage from './UpdateSettingsPage';
import SocialLinksPage from './SocialLinksPage';
import UpdateSocialLinkPage from './UpdateSocialLinkPage';
import FeaturesPage from './FeaturesPage';
import UpdateFeaturePage from './UpdateFeaturePage';

export {
	HomePage,
	UpdateSettingsPage,
	SocialLinksPage,
	UpdateSocialLinkPage,
	FeaturesPage,
	UpdateFeaturePage
}