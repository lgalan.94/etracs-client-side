import { AdminSideNavbar, AdminTopbar, AdminFooter } from '../../components';
import { Header } from './contents';

 const HomePage = () => {
  return (
    <>
    <div className="flex h-screen">
      {/* Side Navigation Bar */}
      
      {/* Main Content */}
      <div className="flex-grow">
        {/* Top Bar */}
        <AdminTopbar />  
        <AdminSideNavbar />
        <p className="px-10 py-6 text-md font-bold text-orange-700 text-xl underline decoration-4 underline-offset-4">Header & Banner Section</p>
        <div className="px-10">
          <Header />
        </div>
      </div>
    </div>
    <AdminFooter />
    </>
  );
};

export default HomePage;