import { SocialLinks } from './contents';
import { AdminSideNavbar, AdminTopbar, AdminFooter } from '../../components';


const SocialLinksPage = () => {
	return (
		<>
		<div className="flex h-screen">
	    <div className="flex-grow">
	      {/* Top Bar */}
	      <AdminTopbar />  
	      <AdminSideNavbar />
	      <p className="px-10 py-6 text-md font-bold text-orange-700 text-xl underline decoration-4 underline-offset-4">Links &amp; Contacts</p>
	      <div className="px-10">
	        <SocialLinks />
	      </div>
	    </div>
	  </div>
	  <AdminFooter />
	  </>
	)
}

export default SocialLinksPage