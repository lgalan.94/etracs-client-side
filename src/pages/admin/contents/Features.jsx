import { useState, useEffect } from 'react';
import { FeatureCard, Loading } from '../../../components';

const Features = () => {
  const [features, setFeatures] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    fetch(`${import.meta.env.VITE_API_URL}/features/all`)
      .then((result) => result.json())
      .then((data) => {
          setFeatures(data.map((feature) => (
            <FeatureCard key={feature._id} featureProp={feature} />
          )));
          setLoading(false);
      });
  }, []);

  return (
      <>
      {
       loading ? 
       <Loading />
       :
       (
        <div className="grid mb-5 grid-cols-1 sm:grid-cols-1 md:grid-cols-1 lg:grid-cols-2 gap-4">
          {features}
        </div>
        )
      }
      </>
  )
};

export default Features;