import { useState, useEffect } from 'react';
import { SocialsCard, Loading } from '../../../components';

const SocialLinks = () => {
  const [socials, setSocials] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    fetch(`${import.meta.env.VITE_API_URL}/socials/get`)
      .then((result) => result.json())
      .then((data) => {
          setSocials(data.map((socials) => (
            <SocialsCard key={socials._id} socialsProp={socials} />
          )));
          setLoading(false);
      });
  }, []);



  return (
     <>
     {
      loading ? <Loading /> : (
         <div className="grid mb-5 grid-cols-1 sm:grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-4">
           {socials}
         </div>
       )
     }
     </>
  )
};

export default SocialLinks;