import Header from './Header&Banner';
import SocialLinks from './SocialLinks';
import Features from './Features';

export {
	Header,
	SocialLinks,
	Features
}