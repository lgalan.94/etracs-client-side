import React, { useState, useEffect } from 'react';
import CustomCard from '../components/CustomCard';
import { getFeatures } from '../utils/API';
import { ImDatabase } from "react-icons/im";   
import { BsFillBuildingFill, BsFillBoxFill } from "react-icons/bs";

const Features = () => {

  return (
    <div className="lg:px-24 lg:py-20 text-slate-500 grid grid-cols-1 md:grid-cols-3 gap-4 mx-auto">
      <CustomCard icon={<ImDatabase size={40} />} cardTitle="Collection System" description="The collection module covers the complete cycle from assigning of accountable forms to collectors then to remittance and liquidation up to deposits." />

      <CustomCard icon={<BsFillBuildingFill size={40} />} cardTitle="Real Property Tax System" description="The Real Property Tax module is the most complex component in the system due to the sheer number of transactions and the amount of data it captures." />

      <CustomCard icon={<BsFillBoxFill size={40} />} cardTitle="Business Permit and Licensing Sytem" description="Highly flexible rule engine that can be customized for any kind of computation of tax, fees, penalties, discounts and requirements based on the LGUs." />
    </div>
  );
};

export default Features;