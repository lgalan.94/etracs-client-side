import { TopBar, NavBar, Banner, Footer } from '../components';
import Features from './Features';
import Members from './Members';

const Home = () => {
	return (
		<>	
			<TopBar />
			<NavBar />
			<Banner />
			<Features />
			<Members />
			<Footer />
		</>
	)
}

export default Home