import React from 'react';
import { CustomTable } from '../components';
import { Aklan, Albay, Bohol, Catanduanes } from '../data.js';

const Members = () => {

  return (
  <>
    <div className="lg:px-40 mb-6">
    <p className="text-center p-10 bg-gray-200 border font-extrabold text-2xl">ETRACS MEMBERS</p>
    </div>
    <div className="lg:px-40 ">
      <p className="text-xl p-4 font-extrabold py-3">LGU's</p>
      <CustomTable title="Aklan" periods={Aklan} />
      <CustomTable title="Albay" periods={Albay} />
      <CustomTable title="Bohol" periods={Bohol} />
      <CustomTable title="Catanduanes" periods={Catanduanes} />
    </div>
  </>
  );
};

export default Members;