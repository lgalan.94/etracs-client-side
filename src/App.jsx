import './App.css'
import React from 'react';
import { Home, About } from './pages';
import Login from './auth/Login';
import Logout from './auth/Logout';
import PrivateRoutes from './utils/ProtectedRoutes';

/*admin pages*/
import { HomePage, 
         UpdateSettingsPage, 
         SocialLinksPage,
         UpdateSocialLinkPage,
         FeaturesPage,
         UpdateFeaturePage } 
from './pages/admin';
/************/

import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { UserProvider } from './UserContext.js';
import 'react-toastify/dist/ReactToastify.css';
import { useState, useEffect } from 'react';

const App = () =>   {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {
      if(localStorage.getItem('token')) {
          fetch(`http://localhost:9000/user/user-details`, {
            method: 'GET',
            headers: {
              Authorization: `Bearer ${localStorage.getItem('token')}`
            }
          })
          .then(result => result.json())
          .then(data => {
              setUser({
                  id: data._id,
                  isAdmin: data.isAdmin
              })
          })
      }

  }, [])
  
  return (
    <UserProvider value = {{ user, setUser, unsetUser }} >
      <BrowserRouter>
        <Routes>
          <Route path="/" element={ <Home /> } />
          <Route path="/about" element={ <About /> } />
          <Route path="/login" element={ <Login /> } />
          <Route path="/logout" element={ <Logout /> } />

          <Route element={<PrivateRoutes />}>
            <Route path="/admin" element={<HomePage />} />
            <Route path="/admin/social-links" element={<SocialLinksPage />} />
            <Route path="/admin/features" element={<FeaturesPage />} />
            <Route path="/update/:settingsId" element={<UpdateSettingsPage />} />
            <Route path="/link-update/:socialId" element={<UpdateSocialLinkPage />} />
            <Route path="/feature-update/:featureId" element={<UpdateFeaturePage />} />
          </Route> 

        </Routes>
      </BrowserRouter>
    </UserProvider>
  );
}

export default App
