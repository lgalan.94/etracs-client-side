import CustomButton from './CustomButton';
import { AiFillEdit } from "react-icons/ai";

const SocialLinksCustomTable = ({ name, social__link }) => {
	return (
		  <tbody>
		      <tr className="text-sm">
		        <td className="border w-1/2 px-4 py-2">{name}</td>
		        <td className="border w-1/2 px-4 py-2">{social__link}</td>
		        <td className="border w-1/2 px-4 text-center py-2">
		        	<CustomButton 
		        		
		        	/>
		        </td>
		      </tr>
		  </tbody>
	)
}

export default SocialLinksCustomTable