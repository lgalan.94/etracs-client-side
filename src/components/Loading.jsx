import React from 'react';

const Loading = () => {
  return (
    <div className="flex items-center justify-center h-screen">
      <div className="animate-spin rounded-full h-20 w-20 border-t-4 border-b-4 border-primary-700">
        <div className="flex items-center justify-center h-full">
          <span className="text-7xl font-bold text-primary-700 transform -rotate-90">E</span>
        </div>
      </div>
    </div>
  );
};

export default Loading;