import { useState, useEffect, useContext } from 'react';
import { NavLink } from 'react-router-dom'; 
import UserContext from '../UserContext';

const NavBar = () => {
	
	const [navbar, setNavbar] = useState(false);
    const { user, setUser } = useContext(UserContext);

    function toggleDropdown(dropdown) {
      dropdown.classList.toggle('hidden');
    }
    
    const dropdowns = document.querySelectorAll('.relative');
    dropdowns.forEach((dropdown) => {
      const button = dropdown.querySelector('a');
      const content = dropdown.querySelector('.dropdown');
      
      button.addEventListener('click', () => {
        toggleDropdown(content);
      });
    });


    useEffect(() => {
        fetch(`${import.meta.env.VITE_API_URL}/user/user-details`, {
          method: 'GET',
          headers: {
            Authorization: `Bearer ${localStorage.getItem('token')}`
          }
        })
        .then(result => result.json())
        .then(data => {
                setUser({
                  id: data._id
                })
        })
    }, [])
	
	return (
		<nav className="sticky bg-white opacity-90 top-0 w-full shadow">
        <div className="justify-between mx-auto lg:max-w-7xl md:items-center md:flex md:px-8">
            <div>
                <div className="flex items-center justify-between md:py-5 md:block">
                    <a href="javascript:void(0)">
                        <img src="https://etracs.org/res/etracs.png" />
                    </a>
                    <div className="md:hidden">
                        <button
                            className="p-2 text-gray-700 rounded-md outline-none focus:border-gray-400 focus:border"
                            onClick={() => setNavbar(!navbar)}
                        >
                            {navbar ? (
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    className="w-6 h-6"
                                    viewBox="0 0 20 20"
                                    fill="currentColor"
                                >
                                    <path
                                        fillRule="evenodd"
                                        d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                                        clipRule="evenodd"
                                    />
                                </svg>
                            ) : (
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    className="w-6 h-6"
                                    fill="none"
                                    viewBox="0 0 24 24"
                                    stroke="currentColor"
                                    strokeWidth={2}
                                >
                                    <path
                                        strokeLinecap="round"
                                        strokeLinejoin="round"
                                        d="M4 6h16M4 12h16M4 18h16"
                                    />
                                </svg>
                            )}
                        </button>
                    </div>
                </div>
            </div>
            <div>
                <div
                    className={`flex-1 justify-self-center pb-3 mt-8 md:block md:pb-0 md:mt-0 ${
                        navbar ? "block" : "hidden"
                    }`}
                >
                  <ul className="items-center text-neutral-600 text-sm font-semibold justify-center space-y-8 md:flex md:space-x-3 md:space-y-0">
                    <li className="hover:border-b-2 hover:text-red-500 border-red-500 pl-4 lg:py-6 lg:px-4">
                      <NavLink as={NavLink} to ='/' className="">Home</NavLink>
                    </li>

                    <li className="hover:border-b-2 hover:text-red-500 border-red-500 pl-4 lg:py-6 lg:px-4">
                      <NavLink as={NavLink} to ='/about' className="">About</NavLink>
                    </li>
                    
                    {
                        user.id !== undefined ? 
                        <li className="hover:border-b-2 hover:text-red-500 border-red-500 pl-4 lg:py-6 lg:px-4">
                          <NavLink as={NavLink} to="/logout">Logout</NavLink>
                        </li>
                        :
                        <li className="hover:border-b-2 hover:text-red-500 border-red-500 pl-4 lg:py-6 lg:px-4">
                          <NavLink as={NavLink} to="/login">Login</NavLink>
                        </li>
                    }
                    
                  </ul>
                </div>
            </div>
        </div>
    </nav>
	)
}

export default NavBar