import { FaYoutube, FaFacebookF, FaTwitter, FaGooglePlusG, FaLinkedinIn } from "react-icons/fa";
import { MdOutlineEmail, MdLocationOn } from "react-icons/md";
import { useState, useEffect } from 'react';
import { getContact, getAllSettings } from '../utils/API';

const TopBar = () => {
  const [address, setAddress] = useState('');
  const [youtube, setYoutube] = useState('');
  const [facebook, setFacebook] = useState('');
  const [twitter, setTwitter] = useState('');
  const [googlePlus, setGooglePlus] = useState('');
  const [linkedin, setLinkedin] = useState('');
  const [website, setWebsite] = useState('');
  const [email, setEmail] = useState('');
  const [contactNumber, setContactNumber] = useState('');

  useEffect(() => {
    getContact()
    .then(data => {
     let ytSetting = data.find((setting) => setting.key === "youtube");
     let fbSetting = data.find((setting) => setting.key === "facebook");
     let twSetting = data.find((setting) => setting.key === "twitter");
     let gpSetting = data.find((setting) => setting.key === "google-plus");
     let liSetting = data.find((setting) => setting.key === "linkedin");
     let wsSetting = data.find((setting) => setting.key === "website");
     let emSetting = data.find((setting) => setting.key === "email");
     let cnSetting = data.find((setting) => setting.key === "contact-number");

     if (ytSetting && fbSetting && twSetting && gpSetting && liSetting && wsSetting && emSetting && cnSetting) {
       setYoutube(ytSetting.value);
       setFacebook(fbSetting.value);
       setTwitter(twSetting.value);
       setGooglePlus(googlePlus.value);
       setLinkedin(liSetting.value);
       setWebsite(wsSetting.value);
       setEmail(emSetting.value);
       setContactNumber(cnSetting.value);
     }

    })
  }, [])

  
  useEffect(() => {
      getAllSettings()
        .then((data) => {
          const addressSetting = data.find((setting) => setting.key === 'address'); 
          if (addressSetting) {
            setAddress(addressSetting.value);
          }
        });
    }, []);


  return (
    <div className="flex items-center text-neutral-600 bg-[#243c5a] justify-center text-white  border-b-2 border-neutral-200 px-20 dark:border-neutral-100 lg:justify-between">
      <div className="mr-1 inline-flex hidden lg:block">
        <ul className="flex text-sm items-center p-0">
          <li className="flex p-1 mr-4">
            <MdLocationOn className="mt-1 me-0.5 text-md" /> {address}
          </li>
          <li className="flex p-1">
            <MdOutlineEmail className="mt-1 me-0.5 text-md" /> {email}
          </li>
        </ul>
      </div>

      <div className="flex justify-center text-neutral-100">
        <a href={youtube} target="_blank" className="p-1.5 hover:text-white hover:bg-red-500">
          <FaYoutube size={18} />
        </a>
        <a href={facebook} target="_blank" className="p-1.5 hover:text-white hover:bg-blue-500">
          <FaFacebookF size={18} />
        </a>
        <a href={twitter} target="_blank" className="p-1.5 hover:text-white hover:bg-[#3ebdb4]">
          <FaTwitter size={18} />
        </a>
        <a href={googlePlus} target="_blank" className="p-1.5 hover:text-white hover:bg-[#e66a74]">
          <FaGooglePlusG size={18} />
        </a>
        <a href={linkedin} target="_blank" className="p-1.5 hover:text-white hover:bg-[#18b9d9]">
          <FaLinkedinIn size={18} />
        </a>
      </div>
    </div>
  );
};

export default TopBar;