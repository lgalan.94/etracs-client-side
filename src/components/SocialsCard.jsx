import React from "react";
import { AiFillEdit } from "react-icons/ai";
import { Link } from 'react-router-dom';

const SocialsCard = (props) => {

 const { _id, key, value } = props.socialsProp;

  return (
    <div className="shadow-md shadow-stone-500 bg-gray-50 text-slate-700">
      <div className="p-2">
        <p className="text-sm font-semibold">{key.toUpperCase()}</p>
      </div>
      <div className="p-2">
        <p className="border text-sm text-center border-gray-300 p-2 font-semibold">{value}</p>
      </div>
      <div className="p-4 mt-auto">
        <Link to={`/link-update/${_id}`}>
          <button className="flex outline outline-slate-300 text-xs hover:bg-gray-500 hover:text-white rounded-sm px-2 py-1 outline-2">
            <AiFillEdit size={14} /> 
          </button>
        </Link>
      </div>
    </div>
  );
};

export default SocialsCard;