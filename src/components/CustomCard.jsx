const CustomCard = ({ icon, cardTitle, description }) => {

  return (
    <div className="flex flex-col p-6 rounded-lg md:flex-row">
      <a className="pr-2 hidden sm:block">{icon}</a>
      <div className="flex flex-col justify-start">
        <h5 className="mb-2 text-xl font-medium text-[#243c5a">{cardTitle}</h5>
        <p className="mb-4 tracking-wide text-justify text-base text-neutral-600">{description}</p>
      </div>
    </div>
  );
};

export default CustomCard;