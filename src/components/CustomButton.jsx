import { BsArrowRightShort } from "react-icons/bs";

const CustomButton = ({ title, containerStyles, icon }) => {
	return (
		<button
			className="flex outline outline-slate-300 text-xs hover:bg-gray-500 hover:text-white rounded-sm px-2 py-1 outline-2"	
			>
			{title}
			{title === "Logout" && <BsArrowRightShort className="mt-[2px]" />}
		</button>
	)
}

export default CustomButton