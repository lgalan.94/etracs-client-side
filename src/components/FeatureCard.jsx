import React from "react";
import { AiFillEdit } from "react-icons/ai";
import { Link } from 'react-router-dom';

const FeatureCard = (props) => {

 const { _id, title, description, icon } = props.featureProp;

  return (
    <div className="shadow-md shadow-stone-500 bg-gray-50 text-slate-700">
      <div className="p-2">
        <p className="text-sm font-semibold">{title.toUpperCase()}</p>
      </div>
      <div className="p-2">
        <p className="border text-sm text-center border-gray-300 p-2 font-semibold">{description}</p>
      </div>
      <div className="p-2">
        <p className="border text-sm text-center border-gray-300 p-2">{icon} </p>
      </div>
      <div className="p-4 mt-auto">
        <Link to={`/feature-update/${_id}`}>
          <button className="flex outline outline-slate-300 text-xs hover:bg-gray-500 hover:text-white rounded-sm px-2 py-1 outline-2">
            <AiFillEdit size={14} /> 
          </button>
        </Link>
      </div>
    </div>
  );
};

export default FeatureCard;