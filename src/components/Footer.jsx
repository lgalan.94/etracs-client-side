import { Link } from 'react-router-dom';
import { FaYoutube, FaFacebookF, FaTwitter, FaGooglePlusG, FaLinkedinIn } from "react-icons/fa";
import { getContact } from '../utils/API';
import { useState, useEffect } from 'react';

const Footer = () => {

 const [youtube, setYoutube] = useState('');
 const [facebook, setFacebook] = useState('');
 const [twitter, setTwitter] = useState('');
 const [googlePlus, setGooglePlus] = useState('');
 const [linkedin, setLinkedin] = useState('');
 const [website, setWebsite] = useState('');
 const [email, setEmail] = useState('');
 const [contactNumber, setContactNumber] = useState('');

 useEffect(() => {
   getContact()
   .then(data => {
    let ytSetting = data.find((setting) => setting.key === "youtube");
    let fbSetting = data.find((setting) => setting.key === "facebook");
    let twSetting = data.find((setting) => setting.key === "twitter");
    let gpSetting = data.find((setting) => setting.key === "google-plus");
    let liSetting = data.find((setting) => setting.key === "linkedin");
    let wsSetting = data.find((setting) => setting.key === "website");
    let emSetting = data.find((setting) => setting.key === "email");
    let cnSetting = data.find((setting) => setting.key === "contact-number");

    if (ytSetting && fbSetting && twSetting && gpSetting && liSetting && wsSetting && emSetting && cnSetting) {
      setYoutube(ytSetting.value);
      setFacebook(fbSetting.value);
      setTwitter(twSetting.value);
      setGooglePlus(googlePlus.value);
      setLinkedin(liSetting.value);
      setWebsite(wsSetting.value);
      setEmail(emSetting.value);
      setContactNumber(cnSetting.value);
    }

   })
 }, [])

  return (
    <footer
      className="bg-neutral-800 text-center text-neutral-600 dark:bg-neutral-600 dark:text-neutral-200 lg:text-left">
      <div
        className="flex items-center justify-center border-b-2 border-neutral-200 p-6 dark:border-neutral-500 lg:justify-between">
        <div className="px-5 text-md text-orange-300 mr-12 hidden lg:block">
          <span>Get connected with us on social networks:</span>
        </div>
        {/* <!-- Social network icons container --> */}
        <div className="flex justify-center text-neutral-100">
          <a href={youtube} target="_blank" className="p-1.5 hover:text-white hover:bg-red-500">
            <FaYoutube size={18} />
          </a>
          <a href={facebook} target="_blank" className="p-1.5 hover:text-white hover:bg-blue-500">
            <FaFacebookF size={18} />
          </a>
          <a href={twitter} target="_blank" className="p-1.5 hover:text-white hover:bg-[#3ebdb4]">
            <FaTwitter size={18} />
          </a>
          <a href={googlePlus} target="_blank" className="p-1.5 hover:text-white hover:bg-[#e66a74]">
            <FaGooglePlusG size={18} />
          </a>
          <a href={linkedin} target="_blank" className="p-1.5 hover:text-white hover:bg-[#18b9d9]">
            <FaLinkedinIn size={18} />
          </a>
        </div>
      </div>
      <div className="mx-6 py-10 px-5 text-center text-neutral-100 md:text-left">
        <div className="grid-1 grid gap-8 md:grid-cols-1 lg:grid-cols-3">
          <div className="">
            <p
              className="mb-4 text-orange-300 flex items-center text-md font-bold uppercase lg:justify-start justify-center">
              GET IN TOUCH
            </p>
            <p className="text-sm lg:text-start md:text-center lg:pr-40">
              Join our mailing list to stay up to date and get notices about our new releases.
            </p>

            <div className="flex lg:justify-start justify-center pt-6">
               <input
                 className="border text-black text-sm border-gray-400 rounded-l px-4 py-2 focus:outline-none focus:ring-2 focus:ring-blue-500"
                 type="text"
                 placeholder="sample@email.com"
               />
               <button
                 className="bg-red-500 text-xs hover:bg-gray-500 hover:text-white font-semibold rounded-r px-4 py-2 ml-1 focus:outline-none focus:ring-2 focus:ring-blue-500"
                 type="button"
               >
                 Send
               </button>
             </div>
          </div>

          <div className="text-sm">
           <a className="">
               <img className="mx-auto mb-4" src="https://etracs.org/res/etracs.png" />
               <p className="text-justify mb-6">
                 <span className="text-orange-300">E-TRACS</span> (Enhanced Tax Revenue Assessment & Collection System) is a free software package for local government units to improve revenue assessments and tax collection.
               </p>
               <div className="text-center">
                <p>Phone Number: <span className="text-orange-200">{contactNumber}</span></p>
                <p>Email: <span className="text-orange-200">{email}</span></p>
                <p>Website: <span className="text-orange-200">{website}</span></p>
               </div>
           </a>
          </div>

          <div className="text-center lg:text-end">
            <p
              className="mb-4 text-orange-300 flex text-md lg:justify-end justify-center font-bold uppercase ">
              Information
            </p>
            <p className="mb-1">
              <Link as={Link} to="/" className="text-neutral-300 text-sm hover:underline hover:text-red-500"
              >Home</Link>
            </p>
            <p className="mb-1">
              <Link as={Link} to="/" className="text-neutral-300 text-sm hover:underline hover:text-red-500"
              >About</Link>
            </p>
            <p className="mb-1">
              <Link as={Link} to="/" className="text-neutral-300 text-sm hover:underline hover:text-red-500"
              >Deployments</Link>
            </p>
            <p className="mb-1">
              <Link as={Link} to="/" className="text-neutral-300 text-sm hover:underline hover:text-red-500"
              >Cloud Services</Link>
            </p>
            <p className="mb-1">
              <Link as={Link} to="/" className="text-neutral-300 text-sm hover:underline hover:text-red-500"
              >Plugins</Link>
            </p>
            <p className="mb-1">
              <Link as={Link} to="/" className="text-neutral-300 text-sm hover:underline hover:text-red-500"
              >Tax Collection Module</Link>
            </p>
            <p className="mb-1">
              <Link as={Link} to="/" className="text-neutral-300 text-sm hover:underline hover:text-red-500"
              >Real Property Tax Module</Link>
            </p>
            <p className="mb-1">
              <Link as={Link} to="/" className="text-neutral-300 text-sm hover:underline hover:text-red-500"
              >Business Permit and Licensing Module</Link>
            </p>
            
          </div>

        </div>
      </div>

      {/* <!--Copyright section--> */}
      <div className="flex justify-between px-20 text-[12px] bg-neutral-900 p-6 text-center text-neutral-200">
        <div>
         <span>© 2023 ALL RIGHTS RESERVED </span>
         <a
           target="_blank"
           className="font-semibold text-orange-300"
           href={website}
         >RAMESES INC. SYSTEM</a>
        </div>
        <div className="text-orange-300">
         <a
           className="ml-3"
           href="https://tailwind-elements.com/"
         >SITEMAP</a>
         <a
           className="ml-3"
           href="https://tailwind-elements.com/"
         >PRIVACY POLICY</a>
         <a
           className="ml-3"
           href="https://tailwind-elements.com/"
         >CONTACT</a>
        </div>
      </div>
    </footer>
  );
}

export default Footer;