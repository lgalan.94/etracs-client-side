import React, { useState, useEffect } from 'react';
import Loading from './Loading';

const Banner = () => {
  const [banner, setBanner] = useState('');
  const [bannerSubtitle, setBannerSubtitle] = useState('');
  const [loading, setLoading] = useState(true);

  useEffect(() => {
      fetch(`${import.meta.env.VITE_API_URL}/settings/settings-all`)
        .then((result) => result.json())
        .then((data) => {
          const bannerSetting = data.find((setting) => setting.key === 'banner'); 
          const bannerSubtitleSetting = data.find((setting) => setting.key === 'banner-subtitle');
          if (bannerSetting && bannerSubtitleSetting) {
            setBanner(bannerSetting.value);
            setBannerSubtitle(bannerSubtitleSetting.value);
          }
          setLoading(false);
        });
    }, []);

  return (
    <div>
      {loading ? (
        <Loading />
      ) : (
        <div className="mx-auto transition-transform p-52 text-center bg-cover" style={{ backgroundImage: "url('https://etracs.org/res/slider/bg1.jpg')" }}>
          <h1 className="banner text-7xl antialiased tracking-widest text-[#db6c30] hidden sm:block mb-6">
            {banner.toUpperCase()}
          </h1>
          <h3 className="banner text-4xl text-neutral-600 tracking-widest hidden sm:block">{bannerSubtitle}</h3>
        </div>
      )}
    </div>
  );
};

export default Banner;