import React from 'react';

const CustomTable = ({ title, periods }) => {
  return (
    <table className="table-auto mb-10 w-full">
      <thead>
        <tr className="text-left text-[#61605c] bg-slate-300 dark:text-neutral-200">
          <th className="w-1/2 px-4 py-2">{title.toUpperCase()}</th>
          <th className="w-1/2 px-4 py-2">Period Covered</th>
        </tr>
      </thead>
      <tbody>
        {periods.map((period) => (
          <tr className="text-sm" key={period.location}>
            <td className=" px-4 py-2">{period.location}</td>
            <td className=" px-4 py-2">{period.covered}</td>
          </tr>
        ))}
      </tbody>
    </table>
  );
};

export default CustomTable;