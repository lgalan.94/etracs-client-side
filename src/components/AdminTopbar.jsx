import { Link } from 'react-router-dom';
import CustomButton from './CustomButton';

const AdminTopbar = () => {
	return (
    <div className="flex sticky top-0 text-slate-700 justify-between bg-orange-400 text-white px-4 py-2">
      <p className="font-extrabold tracking-wide">ETRACS ADMIN PANEL</p>
      <Link as="Link" to="/logout">
        <CustomButton 
        	title="Logout"
        />
      </Link>
    </div>
	)
}

export default AdminTopbar