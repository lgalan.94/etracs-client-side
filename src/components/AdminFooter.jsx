const AdminFooter = () => {
	return (
		<div className="text-sm bg-neutral-200 p-2 text-center">
		  <span>© 2023 Made with ♥ by </span>
		  <a
		  		target="_blank"
		    className="font-semibold text-gray-900"
		    href="https://www.linkedin.com/in/lito-galan-jr-63455b276/"
		  >Lito Galan Jr</a>
		</div>
	)
}

export default AdminFooter