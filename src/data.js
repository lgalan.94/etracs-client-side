export const Aklan = [
  { location: 'Kalibo', covered: 'May 2021 - April 2022' },
  { location: 'Province of Aklan', covered: 'March 2022 - February 2023' },
  { location: 'Altavas', covered: 'January 2022 - December 2022' },
];

export const Albay = [
  { location: 'Legazpi City', covered: 'December 2021 - November 2022' },
  { location: 'Ligao City', covered: 'November 2021 - October 2022' },
];

export const Bohol = [
  { location: 'Loboc', covered: 'December 2020 - November 2021' },
  { location: 'Albuquerque', covered: 'March 2022 - February 2023' },
  { location: 'Tagbilirian', covered: 'December 2021 - November 2022' },
  { location: 'Ubay', covered: 'November 2022 - October 2023' },
  { location: 'Trinidad', covered: 'January 2022 - December 2022' },
];

export const Catanduanes = [
    { location: 'San Andres (Calolbon)', covered: 'August 2021 - July 2022' },
    { location: ' San Miguel', covered: 'November 2021 - October 2022' },
    { location: 'Viga', covered: '  March 2021 - February 2022' },
    { location: 'Virac', covered: 'February 2020 - Jan 2021' },
    { location: 'Catanduanes Province', covered: 'February 2019 - January 2020' },
    { location: 'Bagamanoc', covered: 'February 2019 - January 2020' },
    { location: 'Baras', covered: ' February 2019 - January 2020' },
    { location: 'Pandan', covered: 'February 2019 - January 2020' },
    { location: 'Caramoran', covered: ' February 2019 - January 2020' }
]